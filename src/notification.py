import RPi.GPIO as GPIO
import time
def notify():
	'''
	This method sets up the pi's led, using gpio, then blinks one time and then cleans up.
	'''
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(24 , GPIO.OUT)
	GPIO.output(24 , GPIO.HIGH)
	time.sleep(.5)
	GPIO.output(24 , GPIO.LOW)
	time.sleep(.5)
	GPIO.cleanup()

def cleanup():
	'''
	This method exists to be called externally when a cleanup is needed,
	e.g. it should be called at the ending of the program to make sure the
	led turns off.
	'''
	GPIO.cleanup()
