from msg import Message
import pickle
def write_obj_to_file(file_name, obj):
	'''
	This method uses pickle to write an object, in this case config dictionary
	and message history list, to a file.
	'''
	file = open(file_name, 'wb')
	file.write(pickle.dumps(obj))
	file.close()

def read_obj_from_file(file_name):
	'''
	This method uses pickle to load an object from a file.
	If the file is empty, it returns None.
	'''
	try:
		file = open(file_name, 'rb+')
	except FileNotFoundError:
		file = open(file_name, 'wb+')
	try:
		obj = pickle.loads(file.read())
	except EOFError:
		obj = None
		print('no', file_name, 'found')
	file.close()
	return obj
