import socket
import selectors
import types
import time
import os
import importlib
import tenacity
from msg import *

class MessageReceiveServer:
	RETRY_TIME = .5
	IP = '0.0.0.0'
	def __init__(self, port, gui):
		'''
		Sets a default timeout, a default selector and creates a socket for
		listening. Also sets port and gui attribute.
		'''
		self.timeout = 2 #default timeout, if no timeout needed, timeout is set to None
		self.default_selector = selectors.DefaultSelector()
		self.listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.port = port
		self.gui = gui

	def start_listening(self):
		'''
		Prepares and initializes listening loop of the server.
		'''
		self.bind_address()
		self.listening_socket.listen()
		print('listening on', (self.IP, self.port))
		self.listening_socket.setblocking(False)
		self.default_selector.register(self.listening_socket, selectors.EVENT_READ, data=None)
		self.server_loop()

	@tenacity.retry(wait=tenacity.wait_fixed(RETRY_TIME))
	def bind_address(self):
		'''
		Continually tries to bind the address until no exception occurs.
		For this, tenacity is used.
		'''
		self.listening_socket.bind((self.IP, self.port))

	def server_loop(self):
		"""
		Waits for clients to connect and then listens to client signals.
		"""
		print('start server main loop')
		while True:
			events = self.default_selector.select(timeout=None)
			for key, mask in events:
				if key.data is None:
					self.accept_wrapper(key.fileobj)
				else:
					try:
						self.listen_to_client_signals(key, mask)
					except ConnectionResetError:
						print('One client closed its connection')
					except Exception as err:
						print(err)
			time.sleep(.2)

	def accept_wrapper(self, socket):
		"""
		Accepts new client and defines data type and events for the connection.

		:param socket: the fileobject belonging to the key of the client who wants to connect to server
		"""
		conn, addr = self.listening_socket.accept()
		print('accepted connection from', addr)
		self.gui.show_join_msg(addr[0], other_thread=True)
		conn.setblocking(False)
		data = types.SimpleNamespace(addr=addr, inb=b'', outb=b'')
		events = selectors.EVENT_READ | selectors.EVENT_WRITE
		try:
			self.default_selector.register(conn, events, data=data)
		except KeyError:
			pass

	def listen_to_client_signals(self, key, mask):
		"""
		Listens at the client connection for new messages and processes them.

		:param key: key of the client
		:param mask: mask of the client
		"""
		socket = key.fileobj
		data = key.data
		if mask & selectors.EVENT_READ: #if socket is ready to read
			recv_data = socket.recv(1024)  #1024 is the maximum amount of data to be received at once
			if not recv_data:
				print('closing connection to', data.addr)
				self.default_selector.unregister(socket)
				socket.close()
			else:
				self.read_msg(recv_data)
				time.sleep(.5)
				
	def read_msg(self, data):
		'''
		Reads the received message by putting it into a buffer and
		splitting it at the seperator ":" to extract the length of the message
		which indicates how long the message content will be so that can be
		put into a string and further processed.
		'''
		buffer = ''
		length = None
		buffer += data.decode()
		while True:
			if length is None:
				if ':' not in buffer:
					break
				length_str, ignored, buffer = buffer.partition(':')
				length = int(length_str)
			if len(buffer) < length:
				break
			msg = buffer[:length]
			buffer = buffer[length:]
			length = None
			self.process_msg(msg)
		time.sleep(0.01)

	def process_msg(self, json_str):
		'''
		Creates a message object from the given json string,
		depending on type and content of the message it then performs further
		actions like closing the connections if it's an exit message
		or showing a notification if it's a text message.
		Moreover, it shows the incoming message in the gui and adds it to
		the message history.
		'''
		msg = msg_from_json(json_str)
		print(json_str) # for debugging
		if msg.type == 'notify' and msg.data == 'exit':
			self.close_connections()
		self.gui.show_msg(msg, other_thread=True)
		self.gui.add_msg_to_history(msg)
		if msg.type == 'text':
			self.notify()

	def notify(self):
		'''
		This method lets the led of the pi blink, first checks, whether it's
		really being executed on a pi by checking the architecture.
		'''
		if os.uname()[4].startswith('arm'):
			notification = importlib.import_module('notification')
			notification.notify()

	def close_connections(self):
		'''
		Loops through all connected sockets to close them one by one.
		'''
		events = self.default_selector.select(timeout=self.timeout)
		for key, mask in events:
			if key.data is not None:
				socket = key.fileobj
				socket.close()
