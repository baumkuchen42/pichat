from msg import *
import time
def scan_and_react(string, gui):
	'''
	This method scans a string for a list of trigger words and
	reacts with sending and showing a temperature message if triggered.
	'''
	trigger_words = ['/temp', 'warm', 'hot', 'temperatur', 'Temperatur', 'kalt', 'wetter', 'Wetter', 'cold']
	for word in trigger_words:
		if word in string:
			msg = Message(type='text', data=format_temp(measure_temp()), name='TemperatureBot', timestamp=time.time(), own=True)
			gui.send_msg_wrapper(msg)
			gui.show_msg(msg)
		

def measure_temp():
	'''
	Gets the temperature by first reading the 1-Wire slave list and for each slave
	the file where the temperature values are saved.
	Then extracts a celsius value from said file
	and returns either an average from all existing slaves or the the only
	value if it's only one slave.
	'''
	results = []
	file = open('/sys/devices/w1_bus_master1/w1_master_slaves')
	w1_slaves = file.readlines()
	file.close()
	for line in w1_slaves:
		w1_slave = line.split("\n")[0]
		file = open('/sys/bus/w1/devices/' + str(w1_slave) + '/w1_slave')
		filecontent = file.read()
		stringvalue = filecontent.split("\n")[1].split(" ")[9]
		temperature = float(stringvalue[2:]) / 1000
		results.append(temperature)
	if len(results) == 1:
		return results[0]
	elif len(results) > 1:
		return avg_from_lst(results)

def format_temp(temperature):
	'''
	Converts temperature value to a nice, human readably format.
	'''
	return  '%6.2f °C' % temperature

def avg_from_list(lst):
	'''
	Helper method to evaluate average value.
	'''
	return sum(lst)/len(lst)
