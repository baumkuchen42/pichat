import time
import datetime
import re
from multiprocessing import Process
from threading import Thread
import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, Gdk
import tenacity
import os
import traceback
import importlib

from server import MessageReceiveServer
from client import MessageSendClient
from msg import Message
from file_ops import *
import temperature_bot

class Gui:
	config = {'name':'', 'ip':[], 'port':[], 'own_port':0}
	msg_history = []
	client_list = []
	dialog_grid_last_row_index = None
	server = None
	dialog_builder = NotImplemented
	# fill in with sensible values:
	RETRY_TIME = 2 # in secounds
	WAITING_TIME = .2
	TIMEOUT = 30 # in secounds

	def __init__(self):
		'''
		Initalises a new builder object to import ui file and calls method to show window.
		'''
		self.load_config()
		self.builder = Gtk.Builder()
		self.builder.add_from_file('../ui/main.ui')
		if self.config_filled():
			self.initialize_connections()
			self.show_window()
			self.set_clients_gui_attr()
		else:
			self.show_config_dialog()

######## dialog and window ############################################################

	def show_window(self):
		'''
		Imports window object, creates event handlers, loads history and shows it.
		'''
		window = self.builder.get_object('main_window')
		window.connect('destroy', self.quit_chat)
		self.connect_window_events()
		self.load_history()
		window.show_all()

	def connect_window_events(self):
		'''
		Creates event handlers for buttons and entries in the main window.
		'''
		self.builder.get_object('settings_btn').connect('clicked', self.show_config_dialog, True)
		self.builder.get_object('exit_btn').connect('clicked', self.quit_chat)
		self.builder.get_object('clear_history_btn').connect('clicked', self.on_clear_history_btn_clicked)
		self.builder.get_object('send_btn').connect('clicked', self.on_msg_send)
		self.builder.get_object('msg_entry').connect('activate', self.on_msg_send)
		self.builder.get_object('msg_entry').connect('changed', self.on_msg_entry_text_changed)
		self.builder.get_object('messages_box').connect('size-allocate', self.on_messages_box_size_changed)

	def show_config_dialog(self, widget=None, change_existing_config=False):
		'''
		Constructs the dialog shown at first start up and when clicking on the settings button.
		Fills in already existing values.
		'''
		self.dialog_builder = Gtk.Builder()
		self.dialog_builder.add_from_file('../ui/main.ui')
		self.dialog = self.dialog_builder.get_object('config_dialog')
		if change_existing_config:
			self.connect_dialog_events(change_existing_config=True)
			self.dialog_builder.get_object('dialog_label').set_text('Enter a new user name, change the port or add new IP addresses below.')
			if self.config_filled():
				self.dialog_builder.get_object('name_entry').set_text(self.config.get('name'))
				self.dialog_builder.get_object('own_port_entry').set_text(str(self.config.get('own_port')))
				self.dialog_builder.get_object('ip_label').set_text('Add IP address: ')
				self.add_existing_ips_to_dialog()
		else:
			self.connect_dialog_events()
		self.dialog.show_all()

	def add_existing_ips_to_dialog(self):
		'''
		Helper method for dialog construction.
		'''
		config_grid = self.dialog_builder.get_object('config_grid')
		self.dialog_grid_last_row_index = 2
		existing_ips_title_label = self.import_ui_element('../ui/existing_ips_title.ui', 'existing_ips_title_label')
		self.dialog_grid_last_row_index += 1
		config_grid.insert_row(self.dialog_grid_last_row_index)
		config_grid.attach(existing_ips_title_label, left=0, top=self.dialog_grid_last_row_index, width=3, height=1)
		for ip_address, port in zip(self.config.get('ip'), self.config.get('port')):
			self.dialog_grid_last_row_index += 1
			self.__add_existing_ip(ip_address, port)

	def __add_existing_ip(self, ip_address, port):
		'''
		Helper method for adding an existing ip address plus port to the dialog.
		'''
		config_grid = self.dialog_builder.get_object('config_grid')
		builder = Gtk.Builder()
		builder.add_from_file('../ui/existing_ip.ui')
		box = builder.get_object('existing_ip_box')
		ip_label = builder.get_object('existing_ip_label')
		ip_label.set_text(ip_address)
		port_label = builder.get_object('existing_port_label')
		port_label.set_text(str(port))
		builder.get_object('delete_btn').connect('clicked', self.on_ip_delete_btn_clicked, ip_label, port_label, builder.get_object('sep_label'))
		config_grid.insert_row(self.dialog_grid_last_row_index)
		config_grid.attach(box, left=0, top=self.dialog_grid_last_row_index, width=3, height=1)

	def connect_dialog_events(self, change_existing_config=False):
		'''
		Creates event handlers for dialog.
		Dependent on whether it's the start up config dialog or the settings dialog
		which is used for editing an existing config.
		This is what the parameter change_existing_config is for.
		'''
		if change_existing_config:
			self.dialog.connect('destroy', self.close_dialog)
			self.dialog_builder.get_object('cancel_btn').connect('clicked', self.close_dialog)
			self.dialog_builder.get_object('start_btn').connect('clicked', self.on_start_btn_clicked, False)
		else:
			self.dialog.connect('destroy', self.close_dialog_and_cancel_program)
			self.dialog_builder.get_object('cancel_btn').connect('clicked', self.close_dialog_and_cancel_program)
			self.dialog_builder.get_object('start_btn').connect('clicked', self.on_start_btn_clicked, True)
		self.dialog_builder.get_object('ip_btn').connect('clicked', self.on_ip_btn_clicked)
		self.dialog_builder.get_object('port_entry').connect('activate', self.on_ip_btn_clicked)

####### chat logic ###################################################################################

	def show_msg(self, msg: Message, other_thread=False, from_history=False):
		'''
		Shows message in a different way (using different labels), 
		depending on type and whether from self or others.
		'''
		messages_box = self.builder.get_object('messages_box')
		msg_label = NotImplemented
		user_label = NotImplemented
		msg_box = NotImplemented
		timestamp_label = NotImplemented
		builder = Gtk.Builder()

		if msg.type == 'text':
			if msg.own:
				builder.add_from_file('../ui/own_msg.ui')
				msg_label = builder.get_object('own_msg_label')
				user_label = builder.get_object('own_user_label')
				msg_box = builder.get_object('own_msg_box')
				timestamp_label = self.import_ui_element('../ui/own_timestamp.ui', 'own_timestamp_label')
			else:
				builder.add_from_file('../ui/others_msg.ui')
				msg_label = builder.get_object('others_msg_label')
				user_label = builder.get_object('others_user_label')
				msg_box = builder.get_object('others_msg_box')
				timestamp_label = self.import_ui_element('../ui/others_timestamp.ui', 'others_timestamp_label')
			msg_label.set_text(msg.data)
			user_label.set_text(msg.name + ': ')
			timestamp_label.set_text(self.get_time_from_timestamp(msg.timestamp))
			self.add_element_to_box(messages_box, msg_box, other_thread)
			self.add_element_to_box(messages_box, timestamp_label, other_thread)
			if os.uname()[4].startswith('arm') and not from_history:
				Thread(target=temperature_bot.scan_and_react, args=(msg.data, self), daemon=True).start()

		elif msg.type == 'notify' and msg.data == 'exit':
			self.show_exit_msg(msg.name)

	def show_join_msg(self, ip, other_thread=False):
		'''
		Shows a join message when a new user joins the chat.
		Is called from server when a new client connects to it,
		meaning a new user entered the chat.
		'''
		label = self.import_ui_element('../ui/meta_msg.ui', 'meta_msg_label')
		label.set_text(ip + ' has joined the chat')
		self.add_element_to_box(self.builder.get_object('messages_box'), label, other_thread)

	def show_exit_msg(self, name):
		'''
		Shows an exit message for when a user leaves the chat.
		Not only called when the user properly quits with a leave notification
		but also when someone exits unexpectedly, in this case, the IP address
		is set as parameter instead of a user name.
		'''
		label = self.import_ui_element('../ui/meta_msg.ui', 'meta_msg_label')
		label.set_text(name + ' has left the chat')
		self.add_element_to_box(self.builder.get_object('messages_box'), label)
	
	def load_config(self):
		'''
		Reads the configuration dictionary from its file if existing.
		'''
		config = read_obj_from_file('../data/config')
		if config is not None:
			self.config = config

	def load_history(self):
		'''
		Reads the message history list from its file and loads message by
		message from the list into the chat window.
		'''
		msg_history = read_obj_from_file('../data/msg_history')
		if msg_history is not None:
			self.msg_history = msg_history
			for msg in self.msg_history:
				self.show_msg(msg, from_history=True)

	def add_msg_to_history(self, msg):
		'''
		After sending or receiving a message, this method is called to add
		it to the message history.
		'''
		self.msg_history.append(msg)

	def send_msg_wrapper(self, msg: Message):
		'''
		This method opens a new thread in which is sends the message
		via every client socket.
		'''
		Thread(target=self.__clients_send, args=(msg,)).start()
		self.add_msg_to_history(msg)

	def __clients_send(self, msg=''):
		'''
		Sends the given message via every client in the client list.
		Before it can be sent, the message also needs to be converted to json.
		'''
		for client in self.client_list:
			client.send_msg(msg.to_json())
	
	def initialize_connections(self):
		'''
		Is called on startup and after every potential config change
		to establish a new server, if not already running on the port given in
		the config, and to go through the client list and try to connect to each client.
		'''
		if self.server is not None:
			if self.server.port == self.config.get('own_port'):
				Thread(target=self.try_connect_with_clients, daemon=True).start()
				return
		self.server = MessageReceiveServer(self.config.get('own_port'), self)
		Thread(target=self.server.start_listening, daemon=True).start()
		Thread(target=self.try_connect_with_clients, daemon=True).start()

	def try_connect_with_clients(self):
		'''
		This method loops through all given ip's and port's in the config
		to try to establish a connection to that server. If the connection
		is already existing or if there is an error occuring, e.g. because the
		server is not running yet, it continues with the next ip and port combination.
		'''
		while True:
			for ip, port in zip(self.config.get('ip'), self.config.get('port')):
				if self.is_client_connected((ip, port)):
					continue
				else:
					try:
						client = MessageSendClient((ip, port))
						self.client_list.append(client)
					except (ConnectionRefusedError, TimeoutError):
						continue
			time.sleep(self.WAITING_TIME)

	def quit_chat(self, widget=None):
		'''
		This method quits the chat properly.
		It saves the message history to its file,
		it sends a leave message and closes all sockets, turns off the led if on
		raspberry pi and finally exits the whole program.

		:param widget: when this method gets called via gui event, the calling widget
			is also included as parameter. But not needed for this method.
		'''
		write_obj_to_file('../data/msg_history', self.msg_history)
		self.send_msg_wrapper(Message(type='notify', data='exit', name=self.config.get('name'), timestamp=time.time()))
		self.close_connections()
		if os.uname()[4].startswith('arm'):
			self.turn_off_led()
		sys.exit(0)
	
	def close_connections(self):
		'''
		This method closes the server socket and all client sockets from the list.
		'''
		self.server.close_connections()
		for client in self.client_list:
			client.close_connection()

	def close_dialog_and_cancel_program(self, widget=None):
		'''
		When the startup dialog is closed or cancelled, this method is called.
		'''
		self.dialog.destroy()
		Gtk.main_quit()

	def close_dialog(self, widget=None):
		'''
		When the dialog functions as config editing dialog, this method is called.
		'''
		self.dialog.destroy()

#### events ############################################################################

	def on_msg_send(self, widget):
		'''
		This method is called by hitting enter in the message entry field,
		or clicking the send button.
		It assembles a message object from all given information,
		sends it to all servers it has client sockets connected to
		and finally shows it in the chat window.
		'''
		msg_entry = self.builder.get_object('msg_entry')
		if msg_entry.get_text() != '':
			msg = Message(type='text', data=msg_entry.get_text(), name=self.config.get('name'), timestamp=time.time(), own=True)
			self.send_msg_wrapper(msg)
			self.show_msg(msg)
			msg_entry.set_text('')

	def on_msg_entry_text_changed(self, widget):
		'''
		This method monitors changes in the message entry field and
		sends a typing notification each time that event occurs.
		So basically for every letter that is typed.
		'''
		msg = Message(type='notify', data='typing', name=self.config.get('name'), timestamp=time.time(), own=True)
		self.send_msg_wrapper(msg)

	def on_clear_history_btn_clicked(self, widget):
		'''
		This method visually and logically clears the message history
		by removing all message boxes in the chat and emptying the
		message history list and making the changes permanent by writing
		it to the file.
		'''
		messages_box = self.builder.get_object('messages_box')
		for child in messages_box.get_children():
			messages_box.remove(child)
		self.msg_history = []
		write_obj_to_file('../data/msg_history', self.msg_history)
		
	def on_ip_btn_clicked(self, widget):
		'''
		This method is called by clicking on the plus button next to the ip
		and port entry in the dialog. It checks whether the entries' contents
		are valid and adds them to config dictionary and also visually adds them.
		In case of error, the user is informed by the label next to the entry fields.
		'''
		ip_entry = self.dialog_builder.get_object('ip_entry')
		port_entry = self.dialog_builder.get_object('port_entry')
		if self.is_valid_ip(ip_entry.get_text()) and self.is_valid_port(port_entry.get_text()):
			self.config.get('ip').append(ip_entry.get_text())
			self.config.get('port').append(int(port_entry.get_text()))
			self.dialog_builder.get_object('ip_label').set_text('Next IP address:  ')
			self.__add_existing_ip(ip_entry.get_text(), port_entry.get_text())
			ip_entry.set_text('')
			port_entry.set_text('')
		else:
			self.dialog_builder.get_object('ip_label').set_text('Try again:  ')

	def on_start_btn_clicked(self, widget, show_window:bool):
		'''
		This method is called by clicking on the start button of the dialog.
		It transfers name and the own port to the config and if the config
		is not empty then it saves it, initializes connections and goes to the main window.
		'''
		self.set_name()
		self.set_port()
		print(self.config)
		if self.config_filled():
			write_obj_to_file('../data/config', self.config)
			self.dialog.hide()
			self.initialize_connections()
			if show_window:
				self.show_window()

	def on_messages_box_size_changed(self, widget, event, data=None):
		'''
		This method serves the purpose of automatically scrolling to the last shown
		message in the chat window. It gets triggered by a size change of the box
		the message is added to.
		'''
		adjustment = self.builder.get_object('msg_scroll').get_vadjustment()
		adjustment.set_value(adjustment.get_upper() - adjustment.get_page_size())

	def on_ip_delete_btn_clicked(self, widget, ip_label, port_label, sep_label):
		'''
		This message gets triggered by the delete button next to existing ip and port
		in the settings dialog. It logically and visually removes port and ip.
		'''
		self.config.get('ip').remove(ip_label.get_text())
		self.config.get('port').remove(int(port_label.get_text()))
		ip_label.destroy()
		sep_label.destroy()
		port_label.destroy()
		widget.destroy()

######## helper methods ###########################################################

	def import_ui_element(self, builder_file, element_id):
		'''
		This is a helper method that simplifies the process of freshly adding
		a widget from its own ui file.
		'''
		builder = Gtk.Builder()
		builder.add_from_file(builder_file)
		return builder.get_object(element_id)

	def add_element_to_box(self, box, element, other_thread=False):
		'''
		If a thread other than the main thread wants to add a new message
		in the chat window (such is the case with received messages), it
		needs to use a thread-safe method to do that, namely GLib's idle_add.
		This method manages the distinction of both variants.
		'''
		if other_thread is True:
			GLib.idle_add(box.pack_start, element, False, False, 2)
		else:
			box.pack_start(element, expand=False, fill=False, padding=2)

	def set_name(self):
		'''
		This method checks whether the name is filled in and sets it in the config.
		'''
		name_entry = self.dialog_builder.get_object('name_entry')
		if name_entry.get_text() != '':
			self.config['name'] = name_entry.get_text()

	def set_port(self):
		'''
		This method adds the entered port as own port for the server, if it's valid.
		'''
		own_port_entry = self.dialog_builder.get_object('own_port_entry')
		if self.is_valid_port(own_port_entry.get_text()):
			self.config['own_port'] = int(own_port_entry.get_text())

	def set_clients_gui_attr(self):
		'''
		This method is a workaround that may be not needed but is done for caution reasons.
		It sets this gui as gui attribute for every client in the list.
		'''
		for client in self.client_list:
			client.set_gui(self)

	def get_time_from_timestamp(self, timestamp):
		'''
		This method returns a beautifully formatted string from a given unix timestamp.
		'''
		return datetime.datetime.fromtimestamp(timestamp).strftime('%X')
	
	def config_filled(self):
		'''
		This method checks whether the config dictionary is not missing any required
		components, meaing, name, own port and at least on ip address plus port.
		'''
		return not (self.config.get('name') == '' or self.config.get('own_port') == 0 or self.config.get('ip') == [] or self.config.get('port') == [])

	def is_valid_ip(self, ip:str):
		'''
		This method uses a regex to check if a given ip is valid.
		'''
		return re.match(r'[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}', ip)
	
	def is_valid_port(self, port:str):
		'''
		This uses a regex and a definition area restriction to check whether this is a valid port.
		'''
		return re.match(r'[0-9]{1,5}', port) and (0 < int(port) <= 65535)

	def is_client_connected(self, client_tuple):
		'''
		This method checks whether a given client (identified by a tuple of ip and port)
		is connected, which is recognized by it being in the client list.
		'''
		for client in self.client_list:
			if client.ip_port_tuple == client_tuple:
				return True
		return False

	def turn_off_led(self):
		notification = importlib.import_module('notification')
		notification.cleanup()

#####################################################################

if __name__ == '__main__':
	'''
	When the program is called as standalone script, this part gets executed.
	It properly closes the chat on keyboard interrupt exception and additionally
	prints a stack trace for every other exception that might occur.
	'''
	try:
		Gui()
		Gtk.main()
	except KeyboardInterrupt:
		self.quit_chat()
	except Exception as err:
		traceback.print_tb(err.__traceback__)
		self.quit_chat()
