import json
from collections import OrderedDict
class Message:
	def __init__(self, type: str, data: str, name: str, timestamp: int, own=False):
		'''
		Sets all attributes of the new message object.
		A message object consists of all agreed on parameters plus an
		indicator whether the message is from the person using this app themselves
		or from another user.
		'''
		self.__type = type
		self.__data = data
		self.__name = name
		self.__timestamp = timestamp
		self.__own = own

	@property
	def type(self):
		'''
		A property to allow reading access like this: "object.type" without
		also enabling reading access like it would be the case with the
		standard public attributes.
		'''
		return self.__type

	@property
	def data(self):
		return self.__data

	@property
	def name(self):
		return self.__name
	
	@property
	def timestamp(self):
		return self.__timestamp

	@property
	def own(self):
		return self.__own

	def to_json(self):
		'''
		This method constructs an ordered dictionary of the message object, to
		make sure that the order doesn't get jumbled,
		and then converts it into a json string.
		'''
		return json.dumps(OrderedDict([('type', self.__type), ('data', self.__data), ('name', self.__name), ('timestamp', self.__timestamp)]))

def msg_from_json(json_string):
	'''
	This method extracts the attributes from a given json string and puts
	it into a new Message object.
	'''
	dictionary = json.loads(json_string)
	return Message(dictionary.get("type"), dictionary.get("data"), dictionary.get("name"), int(dictionary.get("timestamp")))
