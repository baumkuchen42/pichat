import socket
import time
class MessageSendClient:
	gui = NotImplemented
	def __init__(self, ip_port_tuple):
		'''
		Initializes the client socket and connects to the server with
		the given ip and port.
		Sets the attribute ip_port_tuple for later use in Client itself
		and for identification from the GUI.
		'''
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect(ip_port_tuple)
		self.ip_port_tuple = ip_port_tuple
	
	def set_gui(self, gui):
		'''
		Sets the gui attribute which is used for showing leave notifications
		in the gui when an exception occurs.
		'''
		self.gui = gui

	def send_msg(self, msg_json):
		'''
		This method gets called by the gui when a message ought to be sent.
		It properly encodes the given json data, adds the header with the message
		length and ":" as seperator.
		When a BrokenPipeError or a ConnectionResetError occurs during the sending
		process that means the server has left so we can print or show in the gui
		a corresponding message.
		'''
		data = msg_json.encode()
		try:
			self.socket.send(str(len(data)).encode() + b':' + data)
		except (BrokenPipeError, ConnectionResetError):
			if self.gui is not NotImplemented:
				self.gui.show_exit_msg(self.ip_port_tuple[0])
			else:
				print(self.ip_port_tuple[0], 'has left the chat')

	def close_connection(self):
		'''
		Closes the socket.
		'''
		self.socket.close()
